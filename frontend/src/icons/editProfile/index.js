import EditPenIcon from "../common/editPen.svg?react";
import QuestionIcon from "../common/question.svg?react";

import FullnameIcon from "./typeEdit/fullname.svg?react";
import BirthdayIcon from "./typeEdit/birthday.svg?react";
import PhoneIcon from "./typeEdit/mobileEdit.svg?react";
import InterestsIcon from "./typeEdit/lightbulb.svg?react";

import LinkedinIcon from "./social/linkedin.svg?react";
import TelegramIcon from "./social/telegram.svg?react";
import ViberIcon from "./social/viber.svg?react";

import MaleIcon from "./gender/male.svg?react";
import FemaleIcon from "./gender/female.svg?react";
import TheyIcon from "./gender/they.svg?react";

export { EditPenIcon, QuestionIcon };
export { FullnameIcon, BirthdayIcon, PhoneIcon, InterestsIcon };
export { LinkedinIcon, TelegramIcon, ViberIcon };
export { MaleIcon, FemaleIcon, TheyIcon };
