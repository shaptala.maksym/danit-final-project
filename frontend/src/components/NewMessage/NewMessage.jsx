import React from 'react';
import NewMessageicon from "../../icons/newMessage.svg?react"
import "./NewMessage.scss"

function NewMessage() {
  return (
    <div className='new-message'>
      <NewMessageicon />
    </div>
  )
}

export default NewMessage;
