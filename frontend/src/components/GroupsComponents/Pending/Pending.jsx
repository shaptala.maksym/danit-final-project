import React from "react";
import "./Pending.scss"
const Pending = (props) => {

    return (
                <div className="pending-wrapper">
                    <div className="text-box">
                        <p className="text">Pending</p>
                    </div>
                </div>
    )
}
export default Pending