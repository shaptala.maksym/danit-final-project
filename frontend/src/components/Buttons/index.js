import Button from "./Button/Button";
import ButtonBase from "./ButtonBase/ButtonBase";
import ButtonClassic from "./ButtonClassic/ButtonClassic";
import ButtonPlateEdit from "./ButtonPlate/ButtonPlateRound/ButtonPlateEdit";
import ButtonPlateCamera from "./ButtonPlate/ButtonPlateRound/ButtonPlateCamera";
import ButtonPlateRectangle from "./ButtonPlate/ButtonPlateRectangle/ButtonPlateRectangle";

export default ButtonBase;
export { Button, ButtonClassic };
export { ButtonPlateEdit, ButtonPlateCamera, ButtonPlateRectangle };
