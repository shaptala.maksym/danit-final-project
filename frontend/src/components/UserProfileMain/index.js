import CoverPhoto from "./components/CoverPhoto/CoverPhoto";
import ProfileBar from "./components/ProfileBar/ProfileBar";
import ProfileNavigation from "./components/ProfileNavigation/ProfileNavigation";

export { CoverPhoto, ProfileBar, ProfileNavigation };
