import React from "react";
import "./GroupItemAside.scss";
import PropTypes from "prop-types";
import cn from "classnames";

const GroupItemAside = (props) => {
  const IMG_URL = import.meta.env.VITE_GROUP_IMG_URL;
  const { coverImageUrl, name, onClick, className } = props;
  
  return (
      <li className={cn('aside-group-item', className)} onClick={onClick}>
            <img 
              className="aside-group-item__img" 
              src={coverImageUrl === '' ? '/images/group/groups-default-cover-photo-2x.png' : `${IMG_URL}/groups/${coverImageUrl}`}
              alt={name} 
            />
            <p className="aside-group-item__title">{name}</p>
      </li>
  );
};

GroupItemAside.propTypes = {
    name: PropTypes.string,
    coverImageUrl: PropTypes.string,
    id: PropTypes.number,
    onClick: PropTypes.func,
};

export default GroupItemAside;
