import React from 'react';
import './HaveNotChats.scss';

const HaveNotChats = () => {
  return (
    <div className='havenot-chats-wrapper'>
      <h3 className='havenot-chats-title'>You haven't chats</h3>
    </div>
  )
}

export default HaveNotChats
