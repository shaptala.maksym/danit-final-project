INSERT INTO feeds (content, post_date,user_id) VALUES
                                                 ('Welcome to our new platform!', '2024-01-01 10:00:00',0),
                                                 ( 'We have updated our system to version 2.0.', '2024-02-15 14:30:00',0),
                                                 ('Join us for our annual event on March 20th.', '2024-03-01 09:00:00',0),
                                                 ('Scheduled maintenance on April 5th from 2 AM to 4 AM.', '2024-04-01 12:00:00',0),
                                                 ( 'Check out our new feature that enhances user experience.', '2024-05-10 16:45:00',0),
                                                 ( 'Happy holidays to all our users!', '2024-12-24 08:30:00',0),
                                                 ( 'Please take a moment to complete our user satisfaction survey.', '2024-06-15 11:20:00',0),
                                                 ( 'We have fixed several bugs reported by users.', '2024-07-05 13:00:00',1),
                                                 ( 'Here are some tips to make the most out of our platform.', '2024-08-20 15:15:00',1),
                                                 ( 'Please review our updated community guidelines.', '2024-09-10 10:45:00',1);
