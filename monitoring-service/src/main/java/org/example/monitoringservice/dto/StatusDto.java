package org.example.monitoringservice.dto;

import lombok.Data;

@Data
public class StatusDto {
   private String status;
}
