package org.example.domain;

public enum FriendRequestStatus {
    PENDING,
    ACCEPTED,
    REJECTED
}
