package org.example.domain;

public enum GroupType {
    OPEN, PRIVATE
}
