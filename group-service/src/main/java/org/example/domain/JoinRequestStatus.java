package org.example.domain;

public enum JoinRequestStatus {
    PENDING, APPROVED, REJECTED
}
