package org.example.domain;

public enum RoleType {
    ADMIN, MODERATOR, MEMBER
}
