-- Create the Schema
CREATE SCHEMA IF NOT EXISTS messaging;

-- Set the Schema Path (optional, ensures you're working within the correct schema)
SET search_path TO messaging;

