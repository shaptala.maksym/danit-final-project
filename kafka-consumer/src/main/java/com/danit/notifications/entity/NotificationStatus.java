package com.danit.notifications.entity;

public enum NotificationStatus {
    NEW,
    PROCESSED
}
