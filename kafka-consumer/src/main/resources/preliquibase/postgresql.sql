-- Create the Schema
CREATE SCHEMA IF NOT EXISTS kafka;

-- Set the Schema Path (optional, ensures you're working within the correct schema)
SET search_path TO kafka;

